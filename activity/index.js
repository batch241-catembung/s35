const express = require ("express");
const mongoose = require("mongoose");

const app = express();

const port = 3001;

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.ix4q3te.mongodb.net/activityS35?retryWrites=true&w=majority",
{
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("connected to mongoDB atlas"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));


const userSchema = new mongoose.Schema({

	name: String,
	password: String
	
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	
			let newUser = new User({
				name:req.body.name,
				password:req.body.password
			});

			newUser.save((saveErr, savedTask)=>{
			if(saveErr)
				{
				return console.error(saveErr);
				}
			else
				{
				return res.status(201).send("new user created");
				}
			})	

});


//tells our server listen to port
app.listen(port, () => console.log(`server running at port ${port}`));






