// use the "require" directive to load the express module
const express = require ("express");

//allows creation of schemas to model our data structures
//also has acces to a number of methods for manipulation our database
const mongoose = require("mongoose");

//create  ab application express
const app = express();

//for our application server to run, we need a port to listen to
const port = 3000;


//mongo db connection
/*
	mongoose.connect("<MongoDB connection string>, {useNewUrlParser : true}")
*/

//connection to mongoDB atlas
//add password and database name
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.ix4q3te.mongodb.net/s35?retryWrites=true&w=majority",
{
	//due to update in mongoDB driver that allow connection to it, the default connection is being flagged as error
	//allows us to avoid any current and future errors while connecting to mongoDB
	useNewUrlParser : true,
	useUnifiedTopology : true
});
//connecting to mongoDB locally
let db = mongoose.connection;
//if the connection error occured, print the output in terminal
db.on("error", console.error.bind(console, "connection error"));
//if the connection is successfull, print the merssage "connected to mongoDB atlas"
db.once("open", () => console.log("connected to mongoDB atlas"));



//allow your app to read json data
app.use(express.json());

//allow your app to read other data types
//{extended: true} - by applying, it allows us to receive information in other data types
app.use(express.urlencoded({extended: true}));


// mongoose schema 

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}

});


//models 
// Models must be in singular form and capitalized 
//first parameter - indicate collection in where to store data
//second parameter - specify the schema/blueprint of the documents that will be stored in the mongodb collection
const Task = mongoose.model("Task", taskSchema);


//creating na new task
/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) =>{
	//to check if there are duplicate task
	Task.findOne({name: req.body.name}, (err, result) =>{
		if (result != null && result.name == req.body.name){
			return res.send("Duplicate task found!");
		}
		else
		{
			let newTask = new Task({
				name:req.body.name
			});

			newTask.save((saveErr, savedTask)=>{
			if(saveErr)
				{
				return console.error(saveErr);
				}
			else
				{
				return res.status(201).send("new task created");
				}
			})	
		}
	})


});

//get all the task
/*
Business Logic
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/


app.get("/tasks", (req, res)=>{

	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			});
		};
	});

});










//tells our server listen to port
app.listen(port, () => console.log(`server running at port ${port}`));
